# test-images

## Licenses

- `color.png` Copyright © Sophie Herold, 2009, [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- `exif.png` [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
- `icon.png` [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)